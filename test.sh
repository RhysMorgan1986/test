#!/bin/bash

## Dependency
## sudo apt-get install -y xmlstarlet

if [ $# -ne 2 ]; then
    echo $0: usage: parseGitLog.sh input_file output_file
    exit 1
fi

input=$1
output=$2

## Normalise the file for standard XML parsing
sed -i 's/\&nbsp;/ /g' $input
sed -i '2d' $input 
sed -i 's/xmlns="http:\/\/www.w3.org\/1999\/xhtml" xml:lang="en"/ /g' $input
sed -i 's/initial-scale=1.0"/initial-scale=1.0"\//g' $input

##Create list of repos

xmlstarlet sel -t -m '//tr/span/td[1]/span[1]/a[../../../td[5]/span[contains(.,"months") or contains(.,"weeks")]]/text()' -c '.' -n  $input > repos.out

## Create list of last updated stamps

xmlstarlet sel -t -m '//tr/span/td[5]/span[../../td[5]/span[contains(.,"months") or contains(.,"weeks")]]/text()' -c '.' -n  $input > dates.out

## Join the files and remove 4 week entries

paste repos.out dates.out | grep -v '4 weeks ago' > $output

## Direct the results to STDOUT

echo "$(cat $output)"

## Tidy up temp files
rm repos.out dates.out
